package CarSample;

public class Car {
	private Producer producer;
	private Engine engine;
	private String color = "default";
	private int vMax = 2147483647;
	
	private int bM = 2147483647;
	private int price;

	public Car(Producer producer) {
		super();
		this.producer = producer;
	}

	public Car(Engine engine) {
		super();
		this.engine = engine;
	}

	public int getPrice() {
		double discountRate = this.producer.getDiscountRate() + 1;
		return (int) (this.price * discountRate);
	}

	// Benzin / Diesel
//	public String getType() {
//		Engine.engType=engType;
//		return this.engType;
//	}
//
//	public double getGasUsage() {
//		this.usage=usage;
//		return usage;
//	}

	public void setColor(String color) {
		this.color = color;
	}

	public void setVMax(int vMax) {
		this.vMax = vMax;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	

}
