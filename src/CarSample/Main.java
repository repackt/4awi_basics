package CarSample;

public class Main {
	public static void main(String[] args) {
		Producer p1 = new Producer("VW", 0.05);
		Producer p2 = new Producer("Opel", 0.09);

		// p1.setDiscountRate(0.08);

		Car c1 = new Car(p2);
		c1.setColor("GalaxyBlue");
		c1.setPrice(10000);

		System.out.println(c1.getPrice());

	}

}
