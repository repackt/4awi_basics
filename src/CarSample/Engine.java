package CarSample;

public class Engine {
	private String engType="default";

	public String getType() {
		return this.engType;
	}

	public void setType(String engType) {
		this.engType = engType;
	}
}
