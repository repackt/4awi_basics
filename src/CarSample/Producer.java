package CarSample;

public class Producer {
	private String pName;
	private String pLand = "world";
	private double discountRate;

	public Producer(String pName, double discountRate) {
		super();
		this.pName = pName;
		this.discountRate = discountRate;
	}

	public String getpLand() {
		return pLand;
	}

	public void setpLand(String pLand) {
		this.pLand = pLand;
	}

	public double getDiscountRate() {
		return discountRate;
	}

	public void setDiscountRate(double discountRate) {
		this.discountRate = discountRate;
	}

	public String getpName() {
		return pName;
	}

}
