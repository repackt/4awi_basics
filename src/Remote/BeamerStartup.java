package Remote;

public class BeamerStartup {
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Battery b1 = new Battery();
		Remote r1 = new Remote(b1);

		b1.setChargingStatus(50);

		Battery b2 = new Battery();
		Remote r2 = new Remote(b2);

		b2.setChargingStatus(40);

		r1.turnOn(true);
		System.out.println("Remote 1 is on:" + r1.hasPower());
		System.out.println("Remote 2 is on:" + r2.hasPower() + "\n Remote 2 has Power:" + r2.hasPower());
	}

}
