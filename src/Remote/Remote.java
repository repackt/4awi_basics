package Remote;

public class Remote {
	private boolean isOn = false;
	private Battery battery;
	
	

	public Remote(Battery battery) {
		super();
		this.battery = battery;
	}

	public boolean turnOn(boolean isOn) {
		this.isOn = true;
		return this.isOn;
	}

	public boolean turnOff(boolean isOn) {
		return this.isOn = false;
	}

	public boolean hasPower() {
		if (battery.getChargingStatus() > 49) {
			return true;
		} else
			return false;
	}

}
