package Remote;

public class Battery {
	private int chargingStatus = 100;

	public int getChargingStatus() {
		return this.chargingStatus;
	}

	public void setChargingStatus(int chargingStatus) {
		this.chargingStatus = chargingStatus;
	}

}
