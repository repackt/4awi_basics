package Rectangle;

public class Rectangle {
	private int a, b;

	public int getA() {
		return this.a;
	}
	
	public int getB() {
		return this.b;
	}
	
	public void setA(int a) {
		this.a = a;
	}
	
	public void setB(int b) {
		this.b = b;
	}
	
	public Rectangle(int a, int b) {
		this.a = a;
		this.b = b;
	}

	public void sayHello() {
		System.out.println("ich bin " + this.a + " breit und " + this.b + " lang");
	}

	public double getArea() {
		double area = a * b;
		return area;
	}

	public int returnA() {
		return this.a;
	}
	
	public int returnB() {
		return this.b;
	}

	public double getCircumference() {
		double cf=(2*a)+(2*b);
		return cf;

	}
}
